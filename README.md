
# INC-OC

This repository contains the simulator, linux kernel and benchmarks presented in the [paper](https://arxiv.org/abs/1909.05349).

```
@online{1909.05349,
Author = {Ayoosh Bansal and Jayati Singh and Yifan Hao and Jen-Yang Wen and Renato Mancuso and Marco Caccamo},
Title = {Cache Where you Want! Reconciling Predictability and Coherent Caching},
Year = {2019},
Eprint = {1909.05349},
Eprinttype = {arXiv},
}
```

## One time setup
Links are provided for broader understanding only. The commands provided are sufficient.

The following command works well for our Ubuntu 18.04 setup. Adapted from : [http://gem5.org/Dependencies](http://gem5.org/Dependencies)



```bash
sudo apt install swig gcc m4 python python-dev     \
libgoogle-perftools-dev mercurial scons g++        \
build-essential zlib1g-dev gcc-arm-linux-gnueabihf \
gcc-aarch64-linux-gnu device-tree-compiler git     \
fakeroot build-essential ncurses-dev xz-utils      \
libssl-dev bc kernel-package gcc-aarch64-linux-gnu \
g++-aarch64-linux-gnu libc6-dev-arm64-cross        \
protobuf-compiler libprotobuf-dev -y
```

[Gem5 Full System Simulation](http://gem5.org/Running_gem5#Full_System_.28FS.29_Mode)

[Prepare File System](http://gem5.org/Introduction#Getting_Additional_Tools_and_Files)

```bash
sudo mkdir -p /dist/m5/system && sudo chmod 777 /dist/m5/system 
cd /dist/m5/system
sudo wget http://www.gem5.org/dist/current/arm/aarch-system-20170616.tar.xz
sudo tar xvf aarch-system-20170616.tar.xz
```
Return back to this repository and run
```bash
cp disks/aarch64-ubuntu-trusty-headless.img \
/dist/m5/system/disks/aarch64-ubuntu-trusty-headless.img
```
## Build
```bash
./scripts/makeall.sh # Builds everything and forces gem5 to build clean
```
## Run tests on Linux over Simulator i.e. Full System Simulation
```bash
./scripts/run_gem5_fs.sh # Boots gem5 in Full System mode, more here: http://gem5.org/Running_gem5#Full_System_.28FS.29_Mode
./scripts/connect_terminal.sh # From another shell, connect to linux serial console
```

Username and password within the booted Ubuntu are both root. The [SPLASH2 benchmark suite](https://github.com/bamos/parsec-benchmark) is modified and can be found in marcobenchmarks/src
```bash
cd bin ## contains the benchmark binaries (FFT, Radix, Water N-Squared, Ocean-CP, LU)
./benchmark_cached  ## run the benchmark with all memory as L1 and L2 cacheable
./benchmark_ocinc  ## (naive INC-OC) run the benchmark with all global variables visible to all threads as INC-OC
./benchmark_ocinc_opt ## run the benchmark with carefully selected variables to be marked as INC-OC
```
## Trace mode
Adapted from [PMSI](https://git.uwaterloo.ca/caesr-pub/pmsi)

Modify gem5/src/cpu/testers/rubytest/Trace.hh
```c
#define TRACE 0 // 1 for Trace mode

// 1 to Force memory requests to be INC-OC.
// This mode doesn't have a true CPU so
// this is the way to control memory types
#define OCINC 1
```
Clean gem5 build required.
./scripts/makeall.sh forces clean build for gem5 so use that.

For custom use we also provide a utility to convert gem5 protocol trace to a format usable by Trace mode.
Note that the file trace.trc in gem5 root is used by trace mode boot.
See gem5/bench_traces/make_trace.py

Run Trace mode
```bash
./scripts/run_gem5_trace.sh
```

## Coherence State Machine
The gem5 coherence state machine format is described [here](http://gem5.org/SLICC)

Our state machine can be viewed by opening html/index.html in any web browser.

## Contact
[Ayoosh Bansal](mailto:ayooshb2@illinois.edu) \
[Jayati Singh](mailto:jayati@illinois.edu)

