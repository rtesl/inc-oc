/**
 * Lock Bench: Lock acquire release latency
 *
 * Copyright (C) 2019 Ayoosh Bansal <ayooshb2@illinois.edu>
 * This file is distributed under the University of Illinois Open Source
 * License. See LICENSE.TXT for details.
 */

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <assert.h>
#include <errno.h>
#include <sched.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#define GEM5 0

#if GEM5
#define HWTHREADS 1
#define FILE_NAME "/dev/mem"
#define MEM_BASE 0x170000000 //0x800000000 //0x170000000
#else
#define HWTHREADS 8
#define FILE_NAME "mmap_file"
#define MEM_BASE 0
#endif

#define NTHREADS (HWTHREADS)
#define BILLION 1000000000L
#define CACHE_LINE 64
#define WORKING_SET 4 * 1024

//Globals
int REPS = 1000;
pthread_barrier_t barr;
pthread_t threads[NTHREADS];
enum times {
    LOCAL,
    L1,
    LOCAL_OCINC,
    OCINC,
    TIMES_COUNT,
};
unsigned long long results[NTHREADS][TIMES_COUNT];

struct thread_args {
    void *l1_lock;
    void *local_ocinc_lock;
    void *ocinc_lock;
};

int thread_id (void)
{
    pthread_t self = pthread_self();
    int i;
    for (i = 0; i < NTHREADS; i++) {
        if (threads[i] == self)
            return i;
    }
    return -1;
}

void sync_threads(char *s) {
    int res;
    res = pthread_barrier_wait(&barr);
    if (res == PTHREAD_BARRIER_SERIAL_THREAD || res == 0) {
        printf("Thread %d reached point %s\n", thread_id(), s);
    } else {
        fprintf(stderr,"Error - pthread_barrier_wait return code: %d\n", res);
        exit(EXIT_FAILURE);
    }
}

unsigned long long time_interval(struct timespec later,  struct timespec earlier)
{
    unsigned long long elapsedTime;
    elapsedTime = BILLION * (later.tv_sec - earlier.tv_sec);      // sec to ns
    elapsedTime += (later.tv_nsec - earlier.tv_nsec);   // ns to ns
    return elapsedTime; // ns to us
}

void set_affinity (int cpu)
{
    cpu_set_t cpuset;
    int s;

    CPU_ZERO(&cpuset);
    CPU_SET(cpu, &cpuset);
    s = pthread_setaffinity_np(pthread_self(), sizeof(cpu_set_t), &cpuset);
    if (s != 0) {
        perror("pthread_setaffinity_np");
        exit(EXIT_FAILURE);
    }
}   

void *thread_entry(void* data)
{ 
    register unsigned int sum, i, j;
    struct thread_args *arguments = (struct thread_args *) data;
    int *l1_lock = (int *)arguments->l1_lock;
    int *local_ocinc_lock = (int *)arguments->local_ocinc_lock;
    int *ocinc_lock = (int *)arguments->ocinc_lock;
    int random, s, local_lock = 0;
    struct timespec start, end;
    unsigned long long local_lock_time, l1_lock_time, ocinc_lock_time, local_ocinc_lock_time;
    unsigned int iterations = WORKING_SET / sizeof(int);

    srand(thread_id());
    random = rand() % NTHREADS + 1;
    set_affinity(thread_id());

    // Sweep to get related values to cache
    while(__sync_lock_test_and_set (&local_lock, 1)) {};
    __sync_lock_release (&local_lock);
    sync_threads("Local Lock");
    clock_gettime(CLOCK_MONOTONIC, &start);
    for (j = 0; j < REPS; j++) {
        while(__sync_lock_test_and_set (&local_lock, 1)) {};
        __sync_lock_release (&local_lock);
    }
    clock_gettime(CLOCK_MONOTONIC, &end);
    local_lock_time = time_interval(end, start);

    // Start all threads together, so wait for them to syncronize at barrier.
    while(__sync_lock_test_and_set (l1_lock, 1)) {};
    __sync_lock_release (l1_lock);
    sync_threads("L1 Lock");
    clock_gettime(CLOCK_MONOTONIC, &start);
    for (j = 0; j < REPS; j++) {
        while(__sync_lock_test_and_set (l1_lock, 1)) {};
        __sync_lock_release (l1_lock);
    }
    clock_gettime(CLOCK_MONOTONIC, &end);
    l1_lock_time = time_interval(end, start);

    // Again syncronise all threads for max contention.
    while(__sync_lock_test_and_set (local_ocinc_lock, 1)) {};
    __sync_lock_release (local_ocinc_lock);
    sync_threads("Local ocinc Lock");
    clock_gettime(CLOCK_MONOTONIC, &start);
    for (j = 0; j < REPS; j++) {
        while(__sync_lock_test_and_set (local_ocinc_lock, 1)) {};
        __sync_lock_release (local_ocinc_lock);
    }
    clock_gettime(CLOCK_MONOTONIC, &end);
    local_ocinc_lock_time = time_interval(end, start);

    // Again syncronise all threads for max contention.
    while(__sync_lock_test_and_set (ocinc_lock, 1)) {};
    __sync_lock_release (ocinc_lock);
    sync_threads("ocinc Lock");
    clock_gettime(CLOCK_MONOTONIC, &start);
    for (j = 0; j < REPS; j++) {
        while(__sync_lock_test_and_set (ocinc_lock, 1)) {};
        __sync_lock_release (ocinc_lock);
    }
    clock_gettime(CLOCK_MONOTONIC, &end);
    ocinc_lock_time = time_interval(end, start);
    //printf("REPS = %d\n", REPS);
    printf("Thread %d, Local = %llu, L1 = %llu, Local ocinc = %llu, ocinc = %llu\n",
            thread_id(), local_lock_time, l1_lock_time, local_ocinc_lock_time, ocinc_lock_time);
    results[thread_id()][LOCAL] = local_lock_time/REPS;
    results[thread_id()][L1] = l1_lock_time/REPS;
    results[thread_id()][LOCAL_OCINC] = local_ocinc_lock_time/REPS;
    results[thread_id()][OCINC] = ocinc_lock_time/REPS;
}

int main(int argc, char *argv[])
{
    int c;

    while ((c = getopt(argc, argv, "r:h")) != -1) {
        switch(c) {
            case 'r' : REPS = strtol(optarg,NULL,0);
                       break;
            case 'h' : printf("Usage   : lock_bench_a64 <options>\n");
                       printf("  -rR   : R = number of repetitions.\n");
                       printf("Default : lock_bench -r1000\n");
                       exit(0);
        }
    }

    void *l1_lock, *ocinc_lock, *local_ocinc_lock;
    unsigned int i;
    unsigned long offset = MEM_BASE;
    struct thread_args thread_args[NTHREADS];
    int memfd, ret;
    unsigned long long local_wc, l1_wc, local_ocinc_wc, ocinc_wc;
    unsigned long long local_avg, l1_avg, local_ocinc_avg, ocinc_avg;
    memfd = open(FILE_NAME, O_RDWR | O_SYNC);
    if (memfd == -1) {
        printf("Can't open %s\n", FILE_NAME);
        exit(0);
    }

    pthread_barrier_init(&barr, NULL, NTHREADS);
#if GEM5
    l1_lock = mmap(0, WORKING_SET, PROT_READ | PROT_WRITE, MAP_SHARED | 0x40, memfd, offset);
#else
    l1_lock = mmap(0, WORKING_SET, PROT_READ | PROT_WRITE, MAP_SHARED, memfd, offset);
#endif
    offset += WORKING_SET;
    *(int *) l1_lock = 0;
#if GEM5
    ocinc_lock = mmap(0, WORKING_SET, PROT_READ | PROT_WRITE, MAP_SHARED | 0x80, memfd, offset);
#else
    ocinc_lock = mmap(0, WORKING_SET, PROT_READ | PROT_WRITE, MAP_SHARED , memfd, offset);
#endif
    offset += WORKING_SET;
    *(int *) ocinc_lock = 0;


    for (i = 0; i < NTHREADS; i++) {
#if GEM5
        local_ocinc_lock = mmap(0, WORKING_SET, PROT_READ | PROT_WRITE, MAP_SHARED | 0x80, memfd, offset);
#else
        local_ocinc_lock = mmap(0, WORKING_SET, PROT_READ | PROT_WRITE, MAP_SHARED, memfd, offset);
#endif
        offset += WORKING_SET;
        *(int *) local_ocinc_lock = 0;
        thread_args[i].l1_lock = l1_lock;
        thread_args[i].ocinc_lock = ocinc_lock;
        thread_args[i].local_ocinc_lock = local_ocinc_lock;
        ret = pthread_create(&(threads[i]), NULL, thread_entry, (void *)&thread_args[i]);
        if(ret) {
            fprintf(stderr,"Error - pthread_create return code: %d\n", ret);
            exit(EXIT_FAILURE);
        }
    
        printf("pthread_create() for thread %d returns: %d\n", i, ret);
    }

    for (i = 0; i < NTHREADS; i++) {
        pthread_join( threads[i], NULL);
    }

    for (i = 0; i < NTHREADS; i++) {
        printf("Thread %2d, Local %llu, L1 %llu, ocinc %llu\n",
                i, results[i][LOCAL], results[i][L1], results[i][OCINC]);
    }

    local_wc = l1_wc = local_ocinc_wc = ocinc_wc = 0;
    local_avg = l1_avg = local_ocinc_avg = ocinc_avg = 0;
    for (i = 0; i < NTHREADS; i++) {
        local_wc = local_wc > results[i][LOCAL] ? local_wc : results[i][LOCAL];
        l1_wc = l1_wc > results[i][L1] ? l1_wc : results[i][L1];
        local_ocinc_wc = local_ocinc_wc > results[i][LOCAL_OCINC] ? local_ocinc_wc : results[i][LOCAL_OCINC];
        ocinc_wc = ocinc_wc > results[i][OCINC] ? ocinc_wc : results[i][OCINC];
        local_avg += results[i][LOCAL];
        l1_avg += results[i][L1];
        local_ocinc_avg += results[i][LOCAL_OCINC];
        ocinc_avg += results[i][OCINC];
    }
    local_avg /= NTHREADS;
    l1_avg /= NTHREADS;
    local_ocinc_avg /= NTHREADS;
    ocinc_avg /= NTHREADS;

    printf("WC: Local = %llu, L1 = %llu, L_ocinc = %llu, ocinc = %llu\nAV: Local = %llu, L1 = %llu, L_ocinc = %llu, ocinc = %llu\n",
            local_wc, l1_wc, local_ocinc_wc, ocinc_wc,
            local_avg, l1_avg, local_ocinc_avg, ocinc_avg);

    exit(EXIT_SUCCESS);
}

