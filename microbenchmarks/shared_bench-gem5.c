/**
 * Shared Bench: Cache Coherence Contention effects
 *
 * Copyright (C) 2019 Ayoosh Bansal <ayooshb2@illinois.edu>
 * This file is distributed under the University of Illinois Open Source
 * License. See LICENSE.TXT for details.
 */


#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <assert.h>
#include <errno.h>
#include <sched.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#define GEM5 1

#if GEM5
#define HWTHREADS 4
#define FILE_NAME "/dev/mem"
#define MEM_BASE 0x800000000
#else
#define HWTHREADS 28
#define FILE_NAME "mmap_file"
#define MEM_BASE 0
#endif

#define L1_CACHE 32 * 1024
#define NTHREADS (HWTHREADS)
#define WORKING_SET (L1_CACHE / 2)

#define BILLION 1000000000L

//Globals
int REPS = 500;
int SHARE_RATIO = 100;
int M_FLAG =0x04;
pthread_barrier_t barr;
pthread_t threads[NTHREADS];
enum times {
    READ,
    WRITE,
    TIMES_COUNT,
};
unsigned long long results[NTHREADS][TIMES_COUNT];

struct thread_args {
    void *shared_array;
    void *private_array;
};

int thread_id (void)
{
    pthread_t self = pthread_self();
    int i;
    for (i = 0; i < NTHREADS; i++) {
        if (threads[i] == self)
            return i;
    }
    return -1;
}

void sync_threads(char *s) {
    int res;
    res = pthread_barrier_wait(&barr);
    if (res == PTHREAD_BARRIER_SERIAL_THREAD || res == 0) {
        printf("Thread %d reached point %s\n", thread_id(), s);
    } else {
        fprintf(stderr,"Error - pthread_barrier_wait return code: %d\n", res);
        exit(EXIT_FAILURE);
    }
}

unsigned long long time_interval(struct timespec later,  struct timespec earlier)
{
    unsigned long long elapsedTime;
    elapsedTime = BILLION * (later.tv_sec - earlier.tv_sec);      // sec to ns
    elapsedTime += (later.tv_nsec - earlier.tv_nsec);   // ns to ns
    return elapsedTime; // ns to us
}

void set_affinity (int cpu)
{
    cpu_set_t cpuset;
    int s;

    CPU_ZERO(&cpuset);
    CPU_SET(cpu, &cpuset);
    s = pthread_setaffinity_np(pthread_self(), sizeof(cpu_set_t), &cpuset);
    if (s != 0) {
        perror("pthread_setaffinity_np");
        exit(EXIT_FAILURE);
    }
}   

void *thread_entry(void* data)
{ 
    register unsigned int sum, i, j;
    struct thread_args *arguments = (struct thread_args *) data;
    int *array = (int *)arguments->shared_array;
    int *private_array = (int *)arguments->private_array;
    int random, s;
    struct timespec start, end;
    unsigned long long write_time, read_time;
    unsigned int iterations = WORKING_SET / sizeof(int);

    srand(thread_id());
    random = rand() % NTHREADS + 1;
    set_affinity(thread_id());

    // Sweep to get related values to cache
    for (j = 0; j < REPS; j++) {
        for (i = 0; i < (iterations * SHARE_RATIO) / 100; i++) {
            *(array + i) = random;
        }

        for (; i < iterations; i++) {
            *(private_array + i) = random;
        }
    }

    // Start all threads together, so wait for them to syncronize at barrier.
    sync_threads("Thread start");

    clock_gettime(CLOCK_MONOTONIC, &start);
    for (j = 0; j < REPS; j++) {
        for (i = 0; i < (iterations * SHARE_RATIO) / 100; i++) {
            *(array + i) = random;
        }

        for (; i < iterations; i++) {
            *(private_array + i) = random;
        }
#if 1
	for (i = 0; i < (iterations * SHARE_RATIO) / 100; i++) {
            sum += *(array + i);
        }

        for (; i < iterations; i++) {
            sum += *(private_array + i);
        }
#endif
    }
    __sync_synchronize();
    clock_gettime(CLOCK_MONOTONIC, &end);

    write_time = time_interval(end, start);

    // Sweep to get related values to cache
    for (j = 0; j < REPS; j++) {
        for (i = 0; i < (iterations * SHARE_RATIO) / 100; i++) {
            sum += *(array + i);
        }

        for (; i < iterations; i++) {
            sum += *(private_array + i);
        }
    }

    // Again syncronise all threads for max contention.
    sync_threads("Write|Read");

    sum = 0;

    clock_gettime(CLOCK_MONOTONIC, &start);
    for (j = 0; j < REPS; j++) {
        for (i = 0; i < (iterations * SHARE_RATIO) / 100; i++) {
            sum += *(array + i);
        }

        for (; i < iterations; i++) {
            sum += *(private_array + i);
        }
    }
    __sync_synchronize();
    clock_gettime(CLOCK_MONOTONIC, &end);

    read_time = time_interval(end, start);

    printf("Thread %d, sum = %u, expected = %d\nWrite %llu, Read %llu\n",
            thread_id(), sum, random * iterations * REPS,
            write_time, read_time);
    results[thread_id()][WRITE] = write_time;
    results[thread_id()][READ] = read_time;
}

/***********************************
 */
int main(int argc, char *argv[]) 

{
    int c;
    while ((c = getopt(argc, argv, "r:s:m:h")) != -1) {
        switch(c) {
            case 'r' : REPS = strtol(optarg,NULL,0);
                       break;
            case 's' : SHARE_RATIO = strtol(optarg,NULL,0);
                       break;
            case 'm' : M_FLAG = strtol(optarg,NULL,0);
                       break;
            case 'h' : printf("Usage   : shared_bench_a64 <options>\n");
                       printf("  -rR   : R = number of repetitions.\n");
                       printf("  -sS   : S = share ratio (%%). \n");
                       printf("  -mM   : M = ocinc (0x80) or ocic (0x40).\n");
                       printf("Default : shared_bench_a64 -r500 -s100 -m4\n");
                       exit(0);
        }
    }

    printf("m %u\n",M_FLAG);
    void *shared_array, *private_array;
    unsigned int i;
    unsigned long offset = MEM_BASE;
    struct thread_args thread_args[NTHREADS];
    int memfd, ret;
    unsigned long long write_wc, read_wc, write_avg, read_avg;
	unsigned int req_per_thread = REPS * WORKING_SET / sizeof(int);
    memfd = open(FILE_NAME, O_RDWR | O_SYNC);
    if (memfd == -1) {
        printf("Can't open %s\n", FILE_NAME);
        exit(0);
    }

    pthread_barrier_init(&barr, NULL, NTHREADS);

#if GEM5
    if(M_FLAG==4)
        shared_array = mmap(0, WORKING_SET, PROT_READ | PROT_WRITE, MAP_SHARED | 0x40, memfd, offset);
    else if(M_FLAG==8)
        shared_array = mmap(0, WORKING_SET, PROT_READ | PROT_WRITE, MAP_SHARED | 0x80, memfd, offset);
#else
    shared_array = mmap(0, WORKING_SET, PROT_READ | PROT_WRITE, MAP_SHARED, memfd, offset);
#endif
    offset += WORKING_SET;

    for (i = 0; i < NTHREADS; i++) {
        thread_args[i].shared_array = shared_array;
#if GEM5
        thread_args[i].private_array = mmap(0, WORKING_SET, PROT_READ | PROT_WRITE, MAP_SHARED |0x40, memfd, offset);
#else
        thread_args[i].private_array = mmap(0, WORKING_SET, PROT_READ | PROT_WRITE, MAP_SHARED, memfd, offset);
#endif
        offset += WORKING_SET;

        ret = pthread_create(&(threads[i]), NULL, thread_entry, (void *)&thread_args[i]);
        if(ret) {
            fprintf(stderr,"Error - pthread_create return code: %d\n", ret);
            exit(EXIT_FAILURE);
        }

        printf("pthread_create() for thread %d returns: %d\n", i, ret);
    }

    for (i = 0; i < NTHREADS; i++) {
        pthread_join(threads[i], NULL);
    }

    for (i = 0; i < NTHREADS; i++) {
        printf("Thread %2d, Write %llu, Read %llu\n",
                i, results[i][WRITE], results[i][READ]);
    }


    write_wc = read_wc = write_avg = read_avg = 0;
    for (i = 0; i < NTHREADS; i++) {
        write_wc = write_wc > results[i][WRITE] ? write_wc : results[i][WRITE];
        read_wc = read_wc > results[i][READ] ? read_wc : results[i][READ];
        write_avg += results[i][WRITE];
        read_avg += results[i][READ];
    }
    write_avg /= NTHREADS;
    read_avg /= NTHREADS;

    printf("WC R = %llu, W = %llu\nAV R = %llu, W = %llu\n",
            read_wc, write_wc, read_avg, write_avg);

    printf("Per request\nWC R = %llu, W = %llu\nAV R = %llu, W = %llu\n",
            read_wc/req_per_thread, write_wc/req_per_thread, read_avg/req_per_thread, write_avg/req_per_thread);
    exit(EXIT_SUCCESS);
}

