export M5_PATH=/home/ayoosh/coherence/fs_gem5_stuff

if ! make -C system/arm/dt ;
then exit $ERRCODE;
fi

if ! make -C system/arm/aarch64_bootloader/ ;
then exit $ERRCODE;
fi

if ! scons SLICC_HTML=TRUE -j14 build/OCINC/gem5.debug ;
then exit $ERRCODE;
fi

if ! scons SLICC_HTML=TRUE -j14 build/OCINC/gem5.opt ;
then exit $ERRCODE;
fi

#if ! aarch64-linux-gnu-gcc t.c -g -static ;
#then exit $ERRCODE;
#fi
