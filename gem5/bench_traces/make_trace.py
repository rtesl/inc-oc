#!/usr/bin/python3

import argparse

parser = argparse.ArgumentParser(description="Create .trc file for single core gem5 ProtocolTrace")
parser.add_argument("-o", "--output_file", default=None, help="Output file name")
parser.add_argument("-i", "--input_file", required=True, help="Input file name")
args = parser.parse_args()
if not args.output_file:
    args.output_file = args.input_file + ".trc"

o_f = open(args.output_file, 'w')

with open(args.input_file, 'r') as handle:
    for line in handle:
        if "line" in line:
            if "Begin" in line:
                words = line.split()
                time = words[0]
                addr = words[5][3:][:-1]
                print(words[8])
                if words[8] == "ST":
                    op = "WR"
                else:
                    op = "RD"

                trc_line = addr + " " + op + " " + time + "\n"
                o_f.write(trc_line)
                #print (trc_line)






o_f.close()

