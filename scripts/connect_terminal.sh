TOP_DIR=$PWD
cd $TOP_DIR/gem5
./util/term/m5term localhost 3456

# The port is printed with the gem5 logs, example:
# system.terminal: Listening for connections on port 3456
# Gem5 documentation : http://gem5.org/M5term
