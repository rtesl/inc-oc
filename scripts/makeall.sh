TOP_DIR=$PWD

cd $TOP_DIR/linux_arm_gem5_incoc
if ! make ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- gem5_defconfig
then exit $ERRCODE;
fi
if ! make ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- -j14
then exit $ERRCODE;
fi

cd $TOP_DIR/gem5
rm -rf build
if ! make -C util/term
then exit $ERRCODE;
fi
if ! make -C system/arm/dt ;
then exit $ERRCODE;
fi
if ! make -C system/arm/aarch64_bootloader/ ;
then exit $ERRCODE;
fi
if ! scons SLICC_HTML=TRUE -j14 build/OCINC/gem5.opt ;
then exit $ERRCODE;
fi
if ! scons SLICC_HTML=TRUE -j14 build/OCINC/gem5.debug ;
then exit $ERRCODE;
fi
