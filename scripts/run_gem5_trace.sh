TOP_DIR=$PWD
cd $TOP_DIR/gem5
if ! ./build/OCINC/gem5.debug --debug-flags=ProtocolTrace ./configs/example/ruby_random_test.py --l1d_size=32kB --l1i_size=32kB --l2_size=1024kB --mem-size=4194304kB --mem-type=SimpleMemory --ruby-clock=2GHz --ruby -n 4
then exit $ERRCODE;
fi
