TOP_DIR=$PWD
cd $TOP_DIR/gem5
if ! ./build/OCINC/gem5.opt  configs/example/fs.py -n 4  --cpu-type=TimingSimpleCPU --kernel=$TOP_DIR/linux_arm_gem5_incoc/vmlinux --machine-type=VExpress_GEM5_V1 --dtb-file=$TOP_DIR/gem5/system/arm/dt/armv8_gem5_v1_4cpu.dtb --disk-image=aarch64-ubuntu-trusty-headless.img --cacheline_size=64 --l1d_size=32kB --l1i_size=32kB --l2_size=1024kB --l1d_assoc=4  --l1i_assoc=2 --l2_assoc=16 --mem-size=4194304kB --mem-type=SimpleMemory --cpu-clock=4.75GHz --ruby-clock=2GHz --ruby
then exit $ERRCODE;
fi
