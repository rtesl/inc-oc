/*************************************************************************/
/*                                                                       */
/*  Copyright (c) 1994 Stanford University                               */
/*                                                                       */
/*  All rights reserved.                                                 */
/*                                                                       */
/*  Permission is given to use, copy, and modify this software for any   */
/*  non-commercial purpose as long as this copyright notice is not       */
/*  removed.  All other uses, including redistribution in whole or in    */
/*  part, are forbidden without prior written permission.                */
/*                                                                       */
/*  This software is provided with absolutely no warranty and no         */
/*  support.                                                             */
/*                                                                       */
/*************************************************************************/

/*************************************************************************/
/*                                                                       */
/*  SPLASH Ocean Code                                                    */
/*                                                                       */
/*  This application studies the role of eddy and boundary currents in   */
/*  influencing large-scale ocean movements.  This implementation uses   */
/*  dynamically allocated four-dimensional arrays for grid data storage. */
/*                                                                       */
/*  Command line options:                                                */
/*                                                                       */
/*     -nN : Simulate NxN ocean.  N must be (power of 2)+2.              */
/*     -pP : P = number of processors.  P must be power of 2.            */
/*     -eE : E = error tolerance for iterative relaxation.               */
/*     -rR : R = distance between grid points in meters.                 */
/*     -tT : T = timestep in seconds.                                    */
/*     -s  : Print timing statistics.                                    */
/*     -o  : Print out relaxation residual values.                       */
/*     -h  : Print out command line options.                             */
/*                                                                       */
/*  Default: OCEAN -n130 -p1 -e1e-7 -r20000.0 -t28800.0                  */
/*                                                                       */
/*  NOTE: This code works under both the FORK and SPROC models.          */
/*                                                                       */
/*************************************************************************/

MAIN_ENV

#define DEFAULT_N      258
#define DEFAULT_P        1
#define DEFAULT_E        1e-7
#define DEFAULT_T    28800.0
#define DEFAULT_R    20000.0
#define UP               0
#define DOWN             1
#define LEFT             2
#define RIGHT            3
#define UPLEFT           4
#define UPRIGHT          5
#define DOWNLEFT         6
#define DOWNRIGHT        7
#define PAGE_SIZE     4096

#define CACHED 1
#define OCINC 0
//OPT_LEVEL 0 : INC-OC BASE, 1: Serial Access 2: Read Only 3: Namesakes
#define OCINC_OPT 0

#include <stdio.h>
#include <math.h>
#include <time.h>

struct multi_struct {
   double err_multi;
} *multi;

struct global_struct {
   int id;
   long starttime;
   long trackstart;
   double psiai;
   double psibi;
} *global;

double ****psi;
double ****psim;
double ***psium;
double ***psilm;
double ***psib;
double ***ga;
double ***gb;
double ****work1;
double ***work2;
double ***work3;
double ****work4;
double ****work5;
double ***work6;
double ****work7;
double ****temparray;
double ***tauz;
double ***oldga;
double ***oldgb;
double *f;
double ****q_multi;
double ****rhs_multi;

struct locks_struct {
   LOCKDEC(idlock)
   LOCKDEC(psiailock)
   LOCKDEC(psibilock)
   LOCKDEC(donelock)
   LOCKDEC(error_lock)
   LOCKDEC(bar_lock)
} *locks;

struct bars_struct {
   BARDEC(iteration)
   BARDEC(gsudn)
   BARDEC(p_setup) 
   BARDEC(p_redph) 
   BARDEC(p_soln) 
   BARDEC(p_subph) 
   BARDEC(sl_prini)
   BARDEC(sl_psini)
   BARDEC(sl_onetime)
   BARDEC(sl_phase_1)
   BARDEC(sl_phase_2)
   BARDEC(sl_phase_3)
   BARDEC(sl_phase_4)
   BARDEC(sl_phase_5)
   BARDEC(sl_phase_6)
   BARDEC(sl_phase_7)
   BARDEC(sl_phase_8)
   BARDEC(sl_phase_9)
   BARDEC(sl_phase_10)
   BARDEC(error_barrier)
} *bars;

void subblock();
void slave();
int log_2(int);
void printerr(char *);

struct ReadOnly {
int nprocs;// = //DEFAULT_P;
double res;// = DEFAULT_R;
double dtau;// = DEFAULT_T;
int im;// = DEFAULT_N;
int jm;
double tolerance;// = DEFAULT_E;
double eig2;
double ysca;
int jmm1;
double pi;
double factjacob;
double factlap;
int numlev;
int *imx;
int *jmx;
double *lev_res;
double *lev_tol;
double *i_int_coeff;
double *j_int_coeff;
int xprocs;
int yprocs;
int *xpts_per_proc;
int *ypts_per_proc;
int minlevel;
int do_stats;// = 0;
int do_output;// = 0;
double h1 ;
double h3 ;
double h ;
double lf ;
double f0 ;
double beta;
double t0 ;
double outday0 ;
double outday3 ;
double maxwork ;
} *ReadOnly;



struct Serial {
  double gpr;
} * Serial;






struct Global_Private {
  char pad[PAGE_SIZE];
  int *rel_num_x;
  int *rel_num_y;
  int *eist;
  int *ejst;
  int *oist;     
  int *ojst;     
  int *rlist;    
  int *rljst;    
  int *rlien;    
  int *rljen;    
  int rownum;
  int colnum;
  int neighbors[8];
  double multi_time;
  double total_time;
} *gp;


void main(argc, argv)

int argc;
char *argv[];

{
   int i;
   int j;
   int k;
   double work_multi;
   int my_num;
   int x_part;
   int y_part;
   int d_size;
   int itemp;
   int jtemp;
   double procsqrt;
   FILE *fileptr;
   int iindex;
   int temp = 0;
   char c;
   double min_total;
       double max_total;
   double avg_total;
   double min_multi;
   double max_multi;
   double avg_multi;
   double min_frac;
   double max_frac;
   double avg_frac;
   int ch;
   extern char *optarg;
   unsigned long computeend;
   unsigned long start;
   //unsigned long off = 0x800000000; //offset for A53
   unsigned long off = 0x170000000; //offset for gem5
   int fd;
   fd = open("/dev/mem", O_RDWR | O_SYNC);
   if (fd == -1) {
        printf("Can't open %s\n", "/dev/mem");
        exit(0);
    }

#if OCINC
   OCINC_MALLOC(ReadOnly, struct ReadOnly, sizeof(struct ReadOnly))
   OCINC_MALLOC(Serial, struct Serial, sizeof(struct Serial));
#else
   C_MALLOC(ReadOnly, struct ReadOnly, sizeof(struct ReadOnly))
   C_MALLOC(Serial, struct Serial, sizeof(struct Serial));
#endif
   ReadOnly->nprocs = DEFAULT_P;
   ReadOnly->res= DEFAULT_R;
   ReadOnly->dtau = DEFAULT_T;
   ReadOnly->im  = DEFAULT_N;
   ReadOnly->tolerance  = DEFAULT_E;
   ReadOnly->do_stats = 0;
   ReadOnly->do_output = 0;
   ReadOnly->h1 =1000.0;
   ReadOnly->h3 = 4000.0;
   ReadOnly->h  = 5000.0;
   ReadOnly->lf  = -5.12e11;
   ReadOnly->f0  = 8.3e-5;
   ReadOnly->beta = 2.0e-11;
   ReadOnly->t0 = 0.5e-4 ;
   ReadOnly->outday0 = 1.0;
   ReadOnly->outday3 = 2.0;
   ReadOnly->maxwork = 10000.0;

   Serial->gpr = 0.02;  

   CLOCK(start)

   while ((ch = getopt(argc, argv, "n:p:e:r:t:soh")) != -1) {
     switch(ch) {
     case 'n': ReadOnly->im = atoi(optarg);
               if (log_2(ReadOnly->im-2) == -1) {
                 printerr("Grid must be ((power of 2)+2) in each dimension\n");
                 exit(-1);
               }
               break;
     case 'p': ReadOnly->nprocs = atoi(optarg);
               if (ReadOnly->nprocs < 1) {
                 printerr("P must be >= 1\n");
                 exit(-1);
               }
               if (log_2(ReadOnly->nprocs) == -1) {
                 printerr("P must be a power of 2\n");
                 exit(-1);
               }
               break;
     case 'e': ReadOnly->tolerance = atof(optarg); break;
     case 'r': ReadOnly->res = atof(optarg); break;
     case 't': ReadOnly->dtau = atof(optarg); break;
     case 's': ReadOnly->do_stats = !ReadOnly->do_stats; break;
     case 'o': ReadOnly->do_output = !ReadOnly->do_output; break;
     case 'h': printf("Usage: OCEAN <options>\n\n");
               printf("options:\n");
               printf("  -nN : Simulate NxN ocean.  N must be (power of 2)+2.\n");
               printf("  -pP : P = number of processors.  P must be power of 2.\n");
               printf("  -eE : E = error tolerance for iterative relaxation.\n");
               printf("  -rR : R = distance between grid points in meters.\n");
               printf("  -tT : T = timestep in seconds.\n");
               printf("  -s  : Print timing statistics.\n");
               printf("  -o  : Print out relaxation residual values.\n");
               printf("  -h  : Print out command line options.\n\n");
               printf("Default: OCEAN -n%1d -p%1d -e%1g -r%1g -t%1g\n",
                       DEFAULT_N,DEFAULT_P,DEFAULT_E,DEFAULT_R,DEFAULT_T);
               exit(0);
               break;
     }
   }

   MAIN_INITENV(,60000000) 

   ReadOnly->jm = ReadOnly->im;
   printf("\n");
   printf("Ocean simulation with W-cycle multigrid solver\n");
   printf("    Processors                         : %1d\n",ReadOnly->nprocs);
   printf("    Grid size                          : %1d x %1d\n",ReadOnly->im,ReadOnly->jm);
   printf("    Grid resolution (meters)           : %0.2f\n",ReadOnly->res);
   printf("    Time between relaxations (seconds) : %0.0f\n",ReadOnly->dtau);
   printf("    Error tolerance                    : %0.7g\n",ReadOnly->tolerance);
   printf("\n");

   ReadOnly->xprocs = 0;
   ReadOnly->yprocs = 0;
   procsqrt = sqrt((double) ReadOnly->nprocs);
   j = (int) procsqrt;
   while ((ReadOnly->xprocs == 0) && (j > 0)) {
     k = ReadOnly->nprocs / j;
     if (k * j == ReadOnly->nprocs) {
       if (k > j) {
         ReadOnly->xprocs = j;
         ReadOnly->yprocs = k;
       } else {
         ReadOnly->xprocs = k;
         ReadOnly->yprocs = j;
       }
     }
     j--;
   }
   if (ReadOnly->xprocs == 0) {
     printerr("Could not find factors for subblocking\n");
     exit(-1);
   }  

   ReadOnly->minlevel = 0;
   itemp = 1;
   jtemp = 1;
   ReadOnly->numlev = 0;
   ReadOnly->minlevel = 0;
   while (itemp < (ReadOnly->im-2)) {
     itemp = itemp*2;
     jtemp = jtemp*2;
     if ((itemp/ReadOnly->yprocs > 1) && (jtemp/ReadOnly->xprocs > 1)) {
       ReadOnly->numlev++;
     }
   }  
   
   if (ReadOnly->numlev == 0) {
     printerr("Must have at least 2 grid points per processor in each dimension\n");
     exit(-1);
   }
#if OCINC
   OCINC_MALLOC(ReadOnly->imx, int, ReadOnly->numlev*sizeof(int))
   OCINC_MALLOC(ReadOnly->jmx, int, ReadOnly->numlev*sizeof(int))
   OCINC_MALLOC(ReadOnly->lev_res, double, ReadOnly->numlev*sizeof(double))
   OCINC_MALLOC(ReadOnly->lev_tol, double, ReadOnly->numlev*sizeof(double))
   OCINC_MALLOC(ReadOnly->i_int_coeff, double, ReadOnly->numlev*sizeof(double))
   OCINC_MALLOC(ReadOnly->j_int_coeff, double, ReadOnly->numlev*sizeof(double))
   OCINC_MALLOC(ReadOnly->xpts_per_proc, int, ReadOnly->numlev*sizeof(int))
   OCINC_MALLOC(ReadOnly->ypts_per_proc, int, ReadOnly->numlev*sizeof(int))
#else
   C_MALLOC(ReadOnly->imx, int, ReadOnly->numlev*sizeof(int))
   C_MALLOC(ReadOnly->jmx, int, ReadOnly->numlev*sizeof(int))
   C_MALLOC(ReadOnly->lev_res, double, ReadOnly->numlev*sizeof(double))
   C_MALLOC(ReadOnly->lev_tol, double, ReadOnly->numlev*sizeof(double))
   C_MALLOC(ReadOnly->i_int_coeff, double, ReadOnly->numlev*sizeof(double))
   C_MALLOC(ReadOnly->j_int_coeff, double, ReadOnly->numlev*sizeof(double))
   C_MALLOC(ReadOnly->xpts_per_proc, int, ReadOnly->numlev*sizeof(int))
   C_MALLOC(ReadOnly->ypts_per_proc, int, ReadOnly->numlev*sizeof(int))   
#endif
   ReadOnly->imx[ReadOnly->numlev-1] = ReadOnly->im;
   ReadOnly->jmx[ReadOnly->numlev-1] = ReadOnly->jm;
   ReadOnly->lev_res[ReadOnly->numlev-1] = ReadOnly->res;
   ReadOnly->lev_tol[ReadOnly->numlev-1] = ReadOnly->tolerance;

   for (i=ReadOnly->numlev-2;i>=0;i--) {
     ReadOnly->imx[i] = ((ReadOnly->imx[i+1] - 2) / 2) + 2;
     ReadOnly->jmx[i] = ((ReadOnly->jmx[i+1] - 2) / 2) + 2;
     ReadOnly->lev_res[i] = ReadOnly->lev_res[i+1] * 2;
   }

   for (i=0;i<ReadOnly->numlev;i++) {
     ReadOnly->xpts_per_proc[i] = (ReadOnly->jmx[i]-2) / ReadOnly->xprocs;
     ReadOnly->ypts_per_proc[i] = (ReadOnly->imx[i]-2) / ReadOnly->yprocs;
   }  
   for (i=ReadOnly->numlev-1;i>=0;i--) {
     if ((ReadOnly->xpts_per_proc[i] < 2) || (ReadOnly->ypts_per_proc[i] < 2)) {
       ReadOnly->minlevel = i+1;
       break;
     }
   }    
 
   for (i=0;i<ReadOnly->numlev;i++) {
     temp += ReadOnly->imx[i];
   }
   temp = 0;
   j = 0;
   for (k=0;k<ReadOnly->numlev;k++) {
     for (i=0;i<ReadOnly->imx[k];i++) {
       j++;
       temp += ReadOnly->jmx[k];
     }
   }

   d_size = ReadOnly->nprocs*sizeof(double ***);
#if OCINC
   OCINC_P_MALLOC(psi, double **** , d_size)
   OCINC_P_MALLOC(psim, double **** , d_size)
   OCINC_P_MALLOC(work1, double **** , d_size)
   OCINC_P_MALLOC(work4, double **** , d_size)
   OCINC_P_MALLOC(work5, double **** , d_size)
   OCINC_P_MALLOC(work7, double **** , d_size)
   OCINC_P_MALLOC(temparray, double **** , d_size)
#else
   C_P_MALLOC(psi, double **** , d_size)
   C_P_MALLOC(psim, double **** , d_size)
   C_P_MALLOC(work1, double **** , d_size)
   C_P_MALLOC(work4, double **** , d_size)
   C_P_MALLOC(work5, double **** , d_size)
   C_P_MALLOC(work7, double **** , d_size)
   C_P_MALLOC(temparray, double **** , d_size)
#endif
   d_size = 2*sizeof(double **);
   for (i=0;i<ReadOnly->nprocs;i++) {
#if OCINC
       OCINC_P_MALLOC(psi[i], double *** , d_size)
       OCINC_P_MALLOC(psim[i], double *** , d_size)
       OCINC_P_MALLOC(work1[i], double *** , d_size)
       OCINC_P_MALLOC(work4[i], double *** , d_size)
       OCINC_P_MALLOC(work5[i], double *** , d_size)
       OCINC_P_MALLOC(work7[i], double *** , d_size)
       OCINC_P_MALLOC(temparray[i], double *** , d_size)
#else
       C_P_MALLOC(psi[i], double *** , d_size)
       C_P_MALLOC(psim[i], double *** , d_size)
       C_P_MALLOC(work1[i], double *** , d_size)
       C_P_MALLOC(work4[i], double *** , d_size)
       C_P_MALLOC(work5[i], double *** , d_size)
       C_P_MALLOC(work7[i], double *** , d_size)
       C_P_MALLOC(temparray[i], double *** , d_size)
#endif
   }

   d_size = ReadOnly->nprocs*sizeof(double **);
#if OCINC
   OCINC_P_MALLOC(psium, double *** , d_size)
   OCINC_P_MALLOC(psilm, double *** , d_size)
   OCINC_P_MALLOC(psib, double *** , d_size)
   OCINC_P_MALLOC(ga, double *** , d_size)
   OCINC_P_MALLOC(gb, double *** , d_size)
   OCINC_P_MALLOC(work2, double *** , d_size)
   OCINC_P_MALLOC(work3, double *** , d_size)
   OCINC_P_MALLOC(work6, double *** , d_size)
   OCINC_P_MALLOC(tauz, double *** , d_size)
   OCINC_P_MALLOC(oldga, double *** , d_size)
   OCINC_P_MALLOC(oldgb, double *** , d_size)
#else
   C_P_MALLOC(psium, double *** , d_size)
   C_P_MALLOC(psilm, double *** , d_size)
   C_P_MALLOC(psib, double *** , d_size)
   C_P_MALLOC(ga, double *** , d_size)
   C_P_MALLOC(gb, double *** , d_size)
   C_P_MALLOC(work2, double *** , d_size)
   C_P_MALLOC(work3, double *** , d_size)
   C_P_MALLOC(work6, double *** , d_size)
   C_P_MALLOC(tauz, double *** , d_size)
   C_P_MALLOC(oldga, double *** , d_size)
   C_P_MALLOC(oldgb, double *** , d_size)
#endif

#if OCINC
   OCINC_P_MALLOC(gp, struct Global_Private*, (ReadOnly->nprocs+1)*sizeof(struct Global_Private))
#else
   C_P_MALLOC(gp, struct Global_Private*, (ReadOnly->nprocs+1)*sizeof(struct Global_Private))
#endif

    for (i=0;i<ReadOnly->nprocs;i++) {
#if OCINC
     OCINC_MALLOC(gp[i].rel_num_x, int, ReadOnly->numlev*sizeof(int))
     OCINC_MALLOC(gp[i].rel_num_y, int, ReadOnly->numlev*sizeof(int))
     OCINC_MALLOC(gp[i].eist, int, ReadOnly->numlev*sizeof(int))
     OCINC_MALLOC(gp[i].ejst, int, ReadOnly->numlev*sizeof(int))
     OCINC_MALLOC(gp[i].oist, int, ReadOnly->numlev*sizeof(int))
     OCINC_MALLOC(gp[i].ojst, int, ReadOnly->numlev*sizeof(int))
     OCINC_MALLOC(gp[i].rlist, int, ReadOnly->numlev*sizeof(int))
     OCINC_MALLOC(gp[i].rljst, int, ReadOnly->numlev*sizeof(int))
     OCINC_MALLOC(gp[i].rlien, int, ReadOnly->numlev*sizeof(int))
     OCINC_MALLOC(gp[i].rljen, int, ReadOnly->numlev*sizeof(int))
#else
     C_MALLOC(gp[i].rel_num_x, int, ReadOnly->numlev*sizeof(int))
     C_MALLOC(gp[i].rel_num_y, int, ReadOnly->numlev*sizeof(int))
     C_MALLOC(gp[i].eist, int, ReadOnly->numlev*sizeof(int))
     C_MALLOC(gp[i].ejst, int, ReadOnly->numlev*sizeof(int))
     C_MALLOC(gp[i].oist, int, ReadOnly->numlev*sizeof(int))
     C_MALLOC(gp[i].ojst, int, ReadOnly->numlev*sizeof(int))
     C_MALLOC(gp[i].rlist, int, ReadOnly->numlev*sizeof(int))
     C_MALLOC(gp[i].rljst, int, ReadOnly->numlev*sizeof(int))
     C_MALLOC(gp[i].rlien, int, ReadOnly->numlev*sizeof(int))
     C_MALLOC(gp[i].rljen, int, ReadOnly->numlev*sizeof(int))
#endif
     gp[i].multi_time = 0;
     gp[i].total_time = 0;
   }

   subblock();

   x_part = (ReadOnly->jm - 2)/ReadOnly->xprocs + 2;
   y_part = (ReadOnly->im - 2)/ReadOnly->yprocs + 2;

   d_size = x_part*y_part*sizeof(double) + y_part*sizeof(double *);
#if OCINC
    OCINC_MALLOC(global, struct global_structm,sizeof(struct global_struct))
#else
    C_MALLOC(global, struct global_structm,sizeof(struct global_struct))
#endif
        for (i=0;i<ReadOnly->nprocs;i++) {

#if OCINC
     OCINC_P_MALLOC(psi[i][0], double **, d_size)
     OCINC_P_MALLOC(psi[i][1], double **, d_size)
     OCINC_P_MALLOC(psim[i][0], double **, d_size)
     OCINC_P_MALLOC(psim[i][1], double **, d_size)
     OCINC_P_MALLOC(psium[i], double **, d_size)
     OCINC_P_MALLOC(psilm[i], double **, d_size)
     OCINC_P_MALLOC(psib[i], double **, d_size)
     OCINC_P_MALLOC(ga[i], double **, d_size)
     OCINC_P_MALLOC(gb[i], double **, d_size)
     OCINC_P_MALLOC(work1[i][0], double **, d_size)
     OCINC_P_MALLOC(work1[i][1], double **, d_size)
     OCINC_P_MALLOC(work2[i], double **, d_size)
     OCINC_P_MALLOC(work3[i], double **, d_size)
     OCINC_P_MALLOC(work4[i][0], double **, d_size)
     OCINC_P_MALLOC(work4[i][1], double **, d_size)
     OCINC_P_MALLOC(work5[i][0], double **, d_size)
     OCINC_P_MALLOC(work5[i][1], double **, d_size)
     OCINC_P_MALLOC(work6[i], double **, d_size) 
     OCINC_P_MALLOC(work7[i][0], double **, d_size)
     OCINC_P_MALLOC(work7[i][1], double **, d_size)
     OCINC_P_MALLOC(temparray[i][0], double **, d_size)
     OCINC_P_MALLOC(temparray[i][1], double **, d_size)
     OCINC_P_MALLOC(tauz[i], double **, d_size)
     OCINC_P_MALLOC(oldga[i], double **, d_size)
     OCINC_P_MALLOC(oldgb[i], double **, d_size)
#else
     C_P_MALLOC(psi[i][0], double **, d_size)
     C_P_MALLOC(psi[i][1], double **, d_size)
     C_P_MALLOC(psim[i][0], double **, d_size)
     C_P_MALLOC(psim[i][1], double **, d_size)
     C_P_MALLOC(psium[i], double **, d_size)
     C_P_MALLOC(psilm[i], double **, d_size)
     C_P_MALLOC(psib[i], double **, d_size)
     C_P_MALLOC(ga[i], double **, d_size)
     C_P_MALLOC(gb[i], double **, d_size)
     C_P_MALLOC(work1[i][0], double **, d_size)
     C_P_MALLOC(work1[i][1], double **, d_size)
     C_P_MALLOC(work2[i], double **, d_size)
     C_P_MALLOC(work3[i], double **, d_size)
     C_P_MALLOC(work4[i][0], double **, d_size)
     C_P_MALLOC(work4[i][1], double **, d_size)
     C_P_MALLOC(work5[i][0], double **, d_size)
     C_P_MALLOC(work5[i][1], double **, d_size)
     C_P_MALLOC(work6[i], double **, d_size) 
     C_P_MALLOC(work7[i][0], double **, d_size)
     C_P_MALLOC(work7[i][1], double **, d_size)
     C_P_MALLOC(temparray[i][0], double **, d_size)
     C_P_MALLOC(temparray[i][1], double **, d_size)
     C_P_MALLOC(tauz[i], double **, d_size)
     C_P_MALLOC(oldga[i], double **, d_size)
     C_P_MALLOC(oldgb[i], double **, d_size)
#endif
        }

#if OCINC
   OCINC_MALLOC(f, double, ReadOnly->im*sizeof(double))
   OCINC_MALLOC(multi, struct multi_struct, sizeof(struct multi_struct))
#else
   C_MALLOC(f, double, ReadOnly->im*sizeof(double))
   C_MALLOC(multi, struct multi_struct, sizeof(struct multi_struct))
#endif

   d_size = ReadOnly->numlev*sizeof(double **);
   if (ReadOnly->numlev%2 == 1) {         /* To make sure that the actual data 
                                   starts double word aligned, add an extra
                                   pointer */
     d_size += sizeof(double **);
   }
   for (i=0;i<ReadOnly->numlev;i++) {
     d_size += ((ReadOnly->imx[i]-2)/ReadOnly->yprocs+2)*((ReadOnly->jmx[i]-2)/ReadOnly->xprocs+2)*sizeof(double)+
              ((ReadOnly->imx[i]-2)/ReadOnly->yprocs+2)*sizeof(double *);
   }

   d_size *= ReadOnly->nprocs;

   if ((ReadOnly->nprocs)%2 == 1) {         /* To make sure that the actual data 
                                   starts double word aligned, add an extra
                                   pointer */
     d_size += sizeof(double ***);
   }

   d_size += ReadOnly->nprocs*sizeof(double ***);

#if OCINC
   OCINC_P_MALLOC(q_multi, double ****, d_size)
   OCINC_P_MALLOC(rhs_multi, double ****, d_size)
#else
   C_P_MALLOC(q_multi, double ****, d_size)
   C_P_MALLOC(rhs_multi, double ****, d_size)
#endif
#if CACHED
   C_MALLOC(locks, struct locks_struct, sizeof(struct locks_struct))
   C_MALLOC(bars, struct bars_struct, sizeof(struct bars_struct))
#else
   OCINC_MALLOC(locks, struct locks_struct, sizeof(struct locks_struct))
   OCINC_MALLOC(bars, struct bars_struct, sizeof(struct bars_struct))
#endif

   LOCKINIT(locks->idlock)
   LOCKINIT(locks->psiailock)
   LOCKINIT(locks->psibilock)
   LOCKINIT(locks->donelock)
   LOCKINIT(locks->error_lock)
   LOCKINIT(locks->bar_lock)

   BARINIT(bars->iteration)
   BARINIT(bars->gsudn)
   BARINIT(bars->p_setup)
   BARINIT(bars->p_redph)
   BARINIT(bars->p_soln)
   BARINIT(bars->p_subph)
   BARINIT(bars->sl_prini)
   BARINIT(bars->sl_psini)
   BARINIT(bars->sl_onetime)
   BARINIT(bars->sl_phase_1)
   BARINIT(bars->sl_phase_2)
   BARINIT(bars->sl_phase_3)
   BARINIT(bars->sl_phase_4)
   BARINIT(bars->sl_phase_5)
   BARINIT(bars->sl_phase_6)
   BARINIT(bars->sl_phase_7)
   BARINIT(bars->sl_phase_8)
   BARINIT(bars->sl_phase_9)
   BARINIT(bars->sl_phase_10)
   BARINIT(bars->error_barrier)

   link_all();

   multi->err_multi = 0.0;
   ReadOnly->i_int_coeff[0] = 0.0;
   ReadOnly->j_int_coeff[0] = 0.0;
   for (i=0;i<ReadOnly->numlev;i++) {
     ReadOnly->i_int_coeff[i] = 1.0/(ReadOnly->imx[i]-1);
     ReadOnly->j_int_coeff[i] = 1.0/(ReadOnly->jmx[i]-1);
   }

/* initialize constants and variables

   id is a global shared variable that has fetch-and-add operations
   performed on it by processes to obtain their pids.   */

   global->id = 0;
   global->psibi = 0.0;
   ReadOnly->pi = atan(1.0);
   ReadOnly->pi = 4.*(ReadOnly->pi);

   ReadOnly->factjacob = -1./(12.*ReadOnly->res*ReadOnly->res);
   ReadOnly->factlap = 1./(ReadOnly->res*ReadOnly->res);
   ReadOnly->eig2 = -ReadOnly->h*ReadOnly->f0*ReadOnly->f0/(ReadOnly->h1*ReadOnly->h3*Serial->gpr);

   ReadOnly->jmm1 = ReadOnly->jm-1 ;
   ReadOnly->ysca = ((double) ReadOnly->jmm1)*(ReadOnly->res) ;

   ReadOnly->im = (ReadOnly->imx[ReadOnly->numlev-1]-2)/ReadOnly->yprocs + 2;
   ReadOnly->jm = (ReadOnly->jmx[ReadOnly->numlev-1]-2)/ReadOnly->xprocs + 2;

   CREATE(slave,ReadOnly->nprocs)
   if (ReadOnly->do_output) {
     printf("                       MULTIGRID OUTPUTS\n");
   }
   WAIT_FOR_END(ReadOnly->nprocs)
   CLOCK(computeend)

   printf("\n");
   printf("                       PROCESS STATISTICS\n");
   printf("                  Total          Multigrid         Multigrid\n");
   printf(" Proc             Time             Time            Fraction\n");
   printf("    0   %15.0f    %15.0f        %10.3f\n",
          gp[0].total_time,gp[0].multi_time,
          gp[0].multi_time/gp[0].total_time);

   if (ReadOnly->do_stats) {
     min_total = max_total = avg_total = gp[0].total_time;
     min_multi = max_multi = avg_multi = gp[0].multi_time;
     min_frac = max_frac = avg_frac = gp[0].multi_time/gp[0].total_time;
     for (i=1;i<ReadOnly->nprocs;i++) {
       if (gp[i].total_time > max_total) {
         max_total = gp[i].total_time;
       }
       if (gp[i].total_time < min_total) {
         min_total = gp[i].total_time;
       }
       if (gp[i].multi_time > max_multi) {
         max_multi = gp[i].multi_time;
       }
       if (gp[i].multi_time < min_multi) {
         min_multi = gp[i].multi_time;
       }
       if (gp[i].multi_time/gp[i].total_time > max_frac) {
         max_frac = gp[i].multi_time/gp[i].total_time;
       }
       if (gp[i].multi_time/gp[i].total_time < min_frac) {
         min_frac = gp[i].multi_time/gp[i].total_time;
       }
       avg_total += gp[i].total_time;
       avg_multi += gp[i].multi_time;
       avg_frac += gp[i].multi_time/gp[i].total_time;
     }
     avg_total = avg_total / ReadOnly->nprocs;
     avg_multi = avg_multi / ReadOnly->nprocs;
     avg_frac = avg_frac / ReadOnly->nprocs;
     for (i=1;i<ReadOnly->nprocs;i++) {
       printf("  %3d   %15.0f    %15.0f        %10.3f\n",
	      i,gp[i].total_time,gp[i].multi_time,
	      gp[i].multi_time/gp[i].total_time);
     }
     printf("  Avg   %15.0f    %15.0f        %10.3f\n",
            avg_total,avg_multi,avg_frac);
     printf("  Min   %15.0f    %15.0f        %10.3f\n",
            min_total,min_multi,min_frac);
     printf("  Max   %15.0f    %15.0f        %10.3f\n",
            max_total,max_multi,max_frac);
   }
   printf("\n");

   global->starttime = start;
   printf("                       TIMING INFORMATION\n");
   printf("Start time                        : %16ld\n",
           global->starttime);
   printf("Initialization finish time        : %16ld\n",
           global->trackstart);
   printf("Overall finish time               : %16ld\n",
           computeend);
   printf("Total time with initialization    : %16ld\n",
           computeend-global->starttime);
   printf("Total time without initialization : %16ld\n",
           computeend-global->trackstart);
   printf("    (excludes first timestep)\n");
   printf("\n");

   MAIN_END
}

int log_2(number)

int number;

{
  int cumulative = 1;
  int out = 0;
  int done = 0;

  while ((cumulative < number) && (!done) && (out < 50)) {
    if (cumulative == number) {
      done = 1;
    } else {
      cumulative = cumulative * 2;
      out ++;
    }
  }

  if (cumulative == number) {
    return(out);
  } else {
    return(-1);
  }
}

void printerr(s)

char *s;

{
  fprintf(stderr,"ERROR: %s\n",s);
}

